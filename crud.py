from sqlalchemy.orm import Session
from models import *

def get_requests(db: Session):
    return db.query(History)

def get_request(db: Session, request_id: int):
    return db.query(History).filter(History.id == request_id).first()

def delete_request(db: Session, request_id: int):
    id = db.query(History).filter(History.id == request_id).delete()
    db.commit()
    return id