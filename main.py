from fastapi import Depends, FastAPI, HTTPException, Security, status
from sqlalchemy.orm import Session

from fastapi_pagination import Page, add_pagination
from fastapi_pagination.ext.sqlalchemy import paginate
from fastapi.security.api_key import APIKeyHeader
from schema import *
import crud
import os
from database import SessionLocal

api_key_header = APIKeyHeader(name="Authorization", auto_error=False,)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

async def is_authenticated(token: str = Security(api_key_header),):
    if token != "Bearer "+os.getenv("API_TOKEN", "CHANGEME"):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    else:
        return True


app = FastAPI(
    title="RustyProxyAPI", 
    version="0.1.1",
    dependencies=[Depends(is_authenticated)]
    )

@app.get("/requests", response_model=Page[History], dependencies=[Depends(is_authenticated)])
def list_requests(db: Session = Depends(get_db)):
    users = crud.get_requests(db)
    return paginate(users)

@app.get("/requests/{request_id}", response_model=History)
def read_request(request_id: int, db: Session = Depends(get_db)):
    req = crud.get_request(db, request_id=request_id)
    if req is None:
        raise HTTPException(status_code=404, detail="Request not found")
    return req

@app.delete("/requests/{request_id}", response_model=DeletedHistory)
def delete_request(request_id: int, db: Session = Depends(get_db)):
    req = crud.delete_request(db, request_id=request_id)
    if req is None:
        raise HTTPException(status_code=404, detail="Request not found")
    return {'id': req}


add_pagination(app)