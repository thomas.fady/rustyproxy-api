FROM debian:11-slim AS builder
RUN apt-get update && \
    apt-get install --no-install-suggests --no-install-recommends --yes python3-venv gcc libpython3-dev && \
    python3 -m venv /venv && \
    /venv/bin/pip install --upgrade pip setuptools wheel

FROM builder AS venv
COPY requirements.txt /requirements.txt
RUN /venv/bin/pip install --disable-pip-version-check -r /requirements.txt

FROM gcr.io/distroless/python3-debian11
COPY --from=venv /venv /venv
COPY . /app
WORKDIR /app
EXPOSE 8000
ENTRYPOINT ["/venv/bin/uvicorn", "main:app", "--host", "0.0.0.0"]