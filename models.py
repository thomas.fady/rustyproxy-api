from sqlalchemy import Boolean, Column, Integer, String, Text

from database import Base


class History(Base):
    __tablename__ = "history"

    id = Column(Integer, primary_key=True, index=True)
    remote_addr = Column(String)
    uri = Column(String)
    method = Column(String)
    status = Column(Integer)
    size = Column(Integer)
    raw = Column(Text)
    ssl = Column(Integer)
    response = Column(Text)
    response_time = Column(Text)