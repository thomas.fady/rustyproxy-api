from pydantic import BaseModel

class History(BaseModel):

    id: int
    remote_addr: str
    uri: str
    method: str
    status: int
    size: int
    raw: str
    ssl: int
    response: str
    response_time: str

    class Config:
        orm_mode = True

class DeletedHistory(BaseModel):
    id: int